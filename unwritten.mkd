Title: Unwritten Packages
CSS: /css/main.css

#{include head}

# Dealing with Unwritten Packages

A while ago Exherbo came up with a solution for the “large number of repositories”
problem. It turns out this solution works rather well, and I’m now convinced that
anyone rushing off and assuming that Git on its own will magically solve anything
other than CVS sucking is going in the wrong direction.

Paludis also has ways of tracking packages installed by hand. Whilst useful on Gentoo,
this really comes into play on Exherbo, where we don’t want to end up with
a massive unmaintainable tree full of packages used by only two people.

The logical next step is tracking packages that don’t exist yet.

An unimaginative person might think that the way to solve this would be to have
a bug for each requested package. But that gets messy — it’s not integrated with
the package manager, and requires considerable extra effort from the user.
A while ago Bernd suggested something more interesting: tracking unwritten
packages in a repository in a similar way to how we do unavailable packages.

### Implications

Most obviously, we can keep track of things we want that haven’t been written yet
in a way that doesn’t involve leaving the package manager to look things up.
We can also use it to track version bumps that we know will take a while.
Doing `paludis --query '>=hugescarypackage-4'`, say, would show that it’s being worked upon.
We can also depend upon things that don’t exist, rather than leaving incomplete
dependency strings around. This is fine in at least two cases — if a dependency
is conditional upon an option that should probably be implemented but isn’t yet,
we can add the option and make it unusable. And we can handle obscure suggested
dependencies (e.g. git has lots of optional dependencies upon weird perl modules,
so we can say “if you want support for git-send-email with SSL, you need to
write such and such a package”). Bored developers could simply `paludis --query '*/*::unwritten'`
and get ideas for what to do.

### Support for it in Paludis

    format = unwritten
    location = /var/db/paludis/repositories/unwritten
    sync = git://git.exherbo.org/unwritten.git
    importance = -100

And see this:

    $ paludis -q genesis
    * sys-apps/genesis
        unwritten:               (1.0)X* {:0} 
        Description:             The daddy of all init systems
        Homepage:                http://www.exherbo.org/
        Comment:                 We need an init system.
        Masked by unwritten:     Package has not been written yet

As with unavailable, installing behaves sensibly:

    $ paludis -pi genesis
    Building target list... 
    Building dependency list...
    Query error:
      * In program paludis -pi genesis:
      * When performing install action from command line:
      * When executing install task:
      * When building dependency list:
      * When adding PackageDepSpec 'sys-apps/genesis':
      * All versions of 'sys-apps/genesis' are masked. Candidates are:
        * sys-apps/genesis-1.0:0::unwritten: Masked by unwritten (Package has not been written yet)

Those interested in the repository format can browse the tree.
The observant might notice that the file format is quite similar to the one used
by unavailable repositories, and wonder whether I was feeling lazy and swiped a
load of code rather than implementing a new repository from scratch.

In the spirit of silly buzzwords, one could argue that this increases distributivity
and democratisation of the repository, since there’s nothing to stop motivated
users from creating their own wishlist repositories. It wouldn’t be hard to make
a simple web interface that lets users request and vote on packages and version bumps,
automatically generating a repository that anyone can use. But fortunately Exherbo
is a dictatorship and has no users, so we don’t have to put up with that kind of nonsense.

--

Copyright 2008 Ciaran McCreesh

#{include CC_3.0_Attribution_ShareAlike}
#{include foot}